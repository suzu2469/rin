module.exports = {
  head: {
    title: "RIN - Develop & Design studio",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "東京都在住のWEBエンジニア" }
    ],
    link: [
      { rel: "icon", type: "image/png", href: "/favicon.png" }
    ]
  },
  loading: { color: "#904399" },
  css: ["@/assets/css/reset.css"],
  modules: [
  ]
}
